package com.example.victor.guessinggame.helpers;

import android.content.Context;
import com.example.victor.guessinggame.dao.GuessingDAO;

/**
 * This class will help to uses the tree
 */
public class TreeHelper {
    //current node information
    public static int current;
    //set the tree's end
    public static boolean endTree;
    //db connection
    public static GuessingDAO db;
    //The last side that the user choose
    public static String lastSide;
    //The animal that the bot answer
    public static String currentAnswer;

    public TreeHelper(Context c) {
        //initialize the values
        db = new GuessingDAO(c);
        current = 1;
        endTree = false;
        lastSide = "";
        currentAnswer = "";
    }

    /**
     * starts the game, getting the first node of the tree
     * @return First Question
     */
    public String getFirst(){
        return db.getQuestion(1);
    }

    /**
     * Try to get the right child. If the node don't have return the right Animal and set the end of tree
     * else return the right Question
     * @return Right Animal Name/Question
     */
    public String getRight(){
        String ret;
        Integer rightId = db.getChild(current, "Right");

        if (rightId == -1){
            ret = db.getAnswer(current, "Right");
            currentAnswer = ret;
            endTree = true;
        }else{
            ret = db.getQuestion(rightId);
            current = rightId;
        }
        lastSide = "Right";
        return ret;
    }

    /**
     * Try to get the left child. If the node don't have return the left Animal and set the end of tree
     * else return the left Question.
     * @return Right Animal Name/Question
     */
    public String getLeft(){
        String ret;
        Integer leftId = db.getChild(current, "Left");

        if (leftId == -1){
            ret = db.getAnswer(current, "Left");
            currentAnswer = ret;
            endTree = true;
        }else{
            ret = db.getQuestion(leftId);
            current = leftId;
        }
        lastSide = "Left";
        return ret;
    }
}
