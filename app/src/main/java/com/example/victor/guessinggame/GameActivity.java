package com.example.victor.guessinggame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.victor.guessinggame.dao.GuessingDAO;
import com.example.victor.guessinggame.helpers.TreeHelper;

/**
 * This class represents the Game with all controls to make it work.
 */
public class GameActivity extends AppCompatActivity {
    //Answer default format(value that is showed to user)
    public static String defaultAnswer = "Is the animal that you thought a/an %s ?";
    //Question default format(value that is showed to user)
    public static String defaultQuestion = "Does the animal that you thought about %s ?";
    //TextView where is showed the results of decisions
    public static TextView questionView;
    //Helper to work with the tree
    public static TreeHelper tree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        //Get TreeHelper instance
        tree = new TreeHelper(GameActivity.this);

        //Get the TextView instance that will show the result of process to user
        questionView = (TextView)findViewById(R.id.game_Question);
        //Get the first data of the tree and set the question text to user.
        //This line will start the Game on the top of the tree.
        questionView.setText(String.format(defaultQuestion, tree.getFirst()));

        //get the Button Yes instance. It will represents the left side of the tree.
        Button btnYes = (Button) findViewById(R.id.game_bntYes);
        //generate the listener on click to button yes
        btnYes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //check if the tree is not in the end
                if(!tree.endTree){
                    //case not in the end, get the left text and show to user
                    //if the current node don't have an left child, the tree is in the end, and
                    //will show the left Animal, else will show the left Trait
                    questionView.setText(String.format(defaultQuestion, tree.getLeft()));
                }else{
                    //The animal that return is correct, and we win.
                    //case in the end, call the win Activity,
                    Intent intentWin = new Intent(GameActivity.this, WinActivity.class);
                    startActivity(intentWin);
                    finish();
                }
            }
        });

        Button btnNo = (Button) findViewById(R.id.game_btnNo);
        btnNo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //check if the tree is not in the end
                if(!tree.endTree){
                    //case not in the end, get the right text and show to user
                    //if the current node don't have an right child, the tree is in the end, and
                    //will show the right Animal, else will show the right Trait
                    questionView.setText(String.format(defaultQuestion, tree.getRight()));
                }else{
                    //The animal that return isn't correct, and we lose.
                    //case in the end, call the lose Activity, that will train the bot
                    Intent intentLose = new Intent(GameActivity.this, LoseActivity.class);
                    //set current values to train
                    intentLose.putExtra("animalName", tree.currentAnswer);
                    intentLose.putExtra("currentId", tree.current + "");
                    intentLose.putExtra("side", tree.lastSide);
                    startActivity(intentLose);
                    finish();
                }
            }
        });
    }
}
