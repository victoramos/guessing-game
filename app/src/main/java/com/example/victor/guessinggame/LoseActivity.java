package com.example.victor.guessinggame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.victor.guessinggame.dao.GuessingDAO;

/**
 * This Activity will be showed if the bot loses the game.
 */
public class LoseActivity extends AppCompatActivity {
    //Trait label default format(value that is showed to user)
    public static String defaultQuestionTrait = "This animal _____ but a/an %s does not (Fill it with an animal trait, like 'Lives in water')";
    //Animal label default format(value that is showed to user)
    public static String defaultQuestionName = "What was the animal that you thought about?";
    //The last animal name of tree
    public static String currentAnimalName;
    //The last row id of tree
    public static Integer currentId;
    //The last side that the user choose
    public static String side;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lose);

        //get the values from the GameActivity
        Intent values = getIntent(); // gets the previously created intent
        currentAnimalName = values.getStringExtra("animalName");
        currentId = Integer.parseInt(values.getStringExtra("currentId"));
        side = values.getStringExtra("side");

        //get the label animal name to set values
        TextView animalNameView = (TextView)findViewById(R.id.lose_animalNameLabel);
        //get the label trait to set values
        TextView animalTraitView = (TextView)findViewById(R.id.lose_animalTraitLabel);

        //set the label value of the last animal that we will get one trait diffent of the animal that the user choose
        animalTraitView.setText(String.format(defaultQuestionTrait, currentAnimalName));
        //set the trait label value
        animalNameView.setText(defaultQuestionName);

        //get the button ok instance
        Button btnOk = (Button) findViewById(R.id.lose_bntOK);
        //set event click listener to ok button
        btnOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //get the db instance
                GuessingDAO db = new GuessingDAO(LoseActivity.this);
                //get the real animal that the user thought
                EditText name =  (EditText)findViewById(R.id.lose_animalName);
                //get the trait that will differentiate the real animal from the animal that the bot answer
                EditText trait =  (EditText)findViewById(R.id.lose_animalTrait);
                //train the with the user's information
                db.train(trait.getText().toString(), name.getText().toString(), currentAnimalName, currentId, side);
                //return to MainActivity to play again
                Intent intentMain = new Intent(LoseActivity.this, MainActivity.class);
                startActivity(intentMain);
                finish();
            }
        });
    }
}
