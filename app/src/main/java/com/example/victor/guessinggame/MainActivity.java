package com.example.victor.guessinggame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.victor.guessinggame.dao.GuessingDAO;

/**
 * This Activity shows the home screen of Game with the option to start.
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the Play button reference
        final Button btnPlay = (Button) findViewById(R.id.main_Play);

        //set the listener to event onClick of play button
        btnPlay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //get the GameActivity intent
                Intent intentGame = new Intent(MainActivity.this, GameActivity.class);
                //starts the Game Activity
                startActivity(intentGame);
                //prevent the user to back to this screen
                finish();
            }
        });
    }
}
