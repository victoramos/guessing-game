package com.example.victor.guessinggame.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * This class will help to connect to database
 */
public class GuessingDAO extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 9;

    // Database Name
    private static final String DATABASE_NAME = "GuessingGame";

    // Guessing table name
    private static final String TABLE_GUESSING = "Guessing";

    public GuessingDAO(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Initialize the database if don't exists
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String _sql = "CREATE TABLE Guessing (Id integer PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                "Question text, LeftChild integer, RightChild integer, LeftAnswer text, " +
                "RightAnswer text)";
        db.execSQL(_sql);
        this.initialize(db);
    }

    /**
     * Upgrade the database when the version change
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GUESSING);

        // Create tables again
        onCreate(db);
    }

    /**
     * Set the default data, to starts the game
     * @param db
     */
    public void initialize(SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put("Id", 1);
        values.put("Question", "Lives in water");
        values.put("LeftAnswer", "Shark");
        values.put("RightAnswer", "Monkey");

        // Inserting Row
        db.insert(TABLE_GUESSING, null, values);
    }

    /**
     * This method will get the child id information of current row
     * @param current current row
     * @param side side that will search
     * @return
     */
    public Integer getChild(int current, String side) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        Integer ret = -1;
        String column = "";

        switch (side){
            case "Left":
                column = "LeftChild";
                break;
            case "Right":
                column = "RightChild";
                break;
        }

        try{
            cursor = db.rawQuery("SELECT * FROM Guessing WHERE Id = ?", new String[] { String.valueOf(current) });

            if(cursor.getCount() > 0) {
                cursor.moveToFirst();

                String child = cursor.getString(cursor.getColumnIndex(column));

                if(child != null) ret = Integer.parseInt(child);

            }

            return ret;
        }finally {
            cursor.close();
        }
    }

    /**
     * This method will get the animal.
     * @param current row that will return the information
     * @param side side that will search
     * @return Animal name that the bot guess
     */
    public String getAnswer(int current, String side) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String empName = "";
        String column = "";

        switch (side){
            case "Left":
                column = "LeftAnswer";
                break;
            case "Right":
                column = "RightAnswer";
                break;
        }

        try{
            cursor = db.rawQuery("SELECT * FROM Guessing WHERE Id = ?", new String[] { String.valueOf(current) });

            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                empName = cursor.getString(cursor.getColumnIndex(column));
            }

            return empName;
        }finally {
            cursor.close();
        }
    }

    /**
     * This method will get the next Trait.
     * @param current row that will return the information
     * @return Animal trait to continue the sequence of tree
     */
    public String getQuestion(int current) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String empName = "";

        try{
            cursor = db.rawQuery("SELECT * FROM Guessing WHERE Id = ?", new String[] { String.valueOf(current) });

            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                empName = cursor.getString(cursor.getColumnIndex("Question"));
            }

            return empName;
        }finally {
            cursor.close();
        }
    }

    /**
     * This method will train the bot.
     * @param trait new trait that will differentiate the last node from the new
     * @param newAnimalName animal that user thought
     * @param currentAnimalName animal that the bot guess
     * @param currentId tree current node
     * @param side last side that user choose
     */
    public void train(String trait, String newAnimalName, String currentAnimalName, Integer currentId, String side) {
        SQLiteDatabase db = this.getWritableDatabase();
        String _sql;
        ContentValues values = new ContentValues();
        values.put("Question", trait);
        values.put("LeftAnswer", newAnimalName);
        values.put("RightAnswer", currentAnimalName);

        //insert the new information from user
        long id = db.insert(TABLE_GUESSING, null, values);

        switch (side){
            case "Right":
                //The tree Adjustment. The new information will be filled on the rightChild of the last node
                _sql = String.format("UPDATE Guessing SET RightChild = %s WHERE Id = %s",id, currentId);
                db.execSQL(_sql);
                break;
            case "Left":
                //The tree Adjustment. The new information will be filled on the leftChild of the last node
                _sql = String.format("UPDATE Guessing SET LeftChild = %s WHERE Id = %s",id, currentId);
                db.execSQL(_sql);
                break;
        }

    }
}
