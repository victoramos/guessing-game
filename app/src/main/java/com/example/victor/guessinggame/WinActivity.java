package com.example.victor.guessinggame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * This method will be showed if the bot win the game.
 */
public class WinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);
        //get the PlayAgain instance
        Button btnPlayAgain = (Button) findViewById(R.id.game_bntPlayAgain);
        btnPlayAgain.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //just return to MainActivity to starts the game again.
                Intent intentMain = new Intent(WinActivity.this, MainActivity.class);
                startActivity(intentMain);
                finish();
            }
        });
    }
}
